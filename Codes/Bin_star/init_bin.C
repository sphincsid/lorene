/*
 * Construction of initial conditions for a binary star computation. 
 * 
 */

/*
 *   Copyright (c) 2021, 2023 Francesco Torsello
 *
 *   Copyright (c) 1999-2001 Eric Gourgoulhon
 *
 *   This file is part of LORENE.
 *
 *   LORENE is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   LORENE is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with LORENE; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

 

/*
 * $Id: init_bin.C,v 1.8 2016/12/05 16:18:23 j_novak Exp $
 * $Log: init_bin.C,v $
 * Revision 1.8  2016/12/05 16:18:23  j_novak
 * Suppression of some global variables (file names, loch, ...) to prevent redefinitions
 *
 * Revision 1.7  2014/10/13 08:53:54  j_novak
 * Lorene classes and functions now belong to the namespace Lorene.
 *
 * Revision 1.6  2014/10/06 15:09:42  j_novak
 * Modified #include directives to use c++ syntax.
 *
 * Revision 1.5  2008/11/14 13:54:53  e_gourgoulhon
 * Reading in input files par_grid*.d the values for the enthalpy at the
 * boundaries between the domains inside the stars.
 *
 * Revision 1.4  2004/03/25 12:35:36  j_novak
 * now using namespace Unites
 *
 * Revision 1.3  2003/01/09 11:07:48  j_novak
 * headcpp.h is now compliant with C++ norm.
 * The include files have been ordered, as well as the local_settings_linux
 *
 * Revision 1.2  2001/12/06 16:18:39  jl_zdunik
 * Return type of main changed from 'void' to 'int'.
 *
 * Revision 1.1.1.1  2001/11/20 15:19:31  e_gourgoulhon
 * LORENE
 *
 * Revision 1.4  2000/03/16  09:47:18  eric
 * Suppression du shift analytique (c'est desormais fait dans coal).
 *
 * Revision 1.3  2000/03/15  13:15:08  eric
 * Utilisation d'une formule analytique pour initialiser le shift a autre
 * chose que zero.
 *
 * Revision 1.2  2000/02/14  14:56:13  eric
 * Construction des EOS par lecture de fichier formate.
 *
 * Revision 1.1  2000/02/03  14:51:35  eric
 * Initial revision
 *
 *
 * $Header: /cvsroot/Lorene/Codes/Bin_star/init_bin.C,v 1.8 2016/12/05 16:18:23 j_novak Exp $
 *
 */

 
// headers C
#include <cstdlib>
#include <cmath>

// headers Lorene
#include "binaire.h"
#include "eos.h"
#include "../../C++/Include/eos_multi_poly.h"
#include "utilitaires.h"
#include "graphique.h"
#include "nbr_spx.h"
#include "unites.h"	    

using namespace Lorene ;

//******************************************************************************

int main(){
    
  using namespace Unites ;

    // Identification of all the subroutines called by the code : 
    
    // system("ident init_bin") ; 

    int nt, np, nz ;
    char blabla[80] ;

    ifstream fich("par_init.d") ;

    //-----------------------------------------------------------------------
    //		Physical parameters imput
    //-----------------------------------------------------------------------

    fich.getline(blabla, 80) ;
    fich.getline(blabla, 80) ;

    int relat_i ; 
    fich >> relat_i; fich.getline(blabla, 80) ;
    bool relat = (relat_i == 1) ; 

    double separ ; 
    fich >> separ; fich.getline(blabla, 80) ;
    separ *= km ;	// translation in machine units

    int irrot1_i, irrot2_i ; 
    double ent_c1, ent_c2 ; 
    fich >> ent_c1 ; fich.getline(blabla, 80) ;
    fich >> irrot1_i ; fich.getline(blabla, 80) ;
    bool irrot1 = (irrot1_i == 1) ; 
    fich >> ent_c2 ; fich.getline(blabla, 80) ;
    fich >> irrot2_i ; fich.getline(blabla, 80) ;
    bool irrot2 = (irrot2_i == 1) ; 
    
    fich.close() ; 
  
    cout << endl << "Requested orbital separation : " << separ / km 
	 << " km" << endl ; 

    //-----------------------------------------------------------------------
    //		Input data for the multi-grid no. 1
    //-----------------------------------------------------------------------
 

    int nzet1 ;
    fich.open("par_grid1.d") ;
    fich.getline(blabla, 80);
    fich.getline(blabla, 80);
    fich >> nz; fich.getline(blabla, 80) ;
    fich >> nzet1; fich.getline(blabla, 80) ;
    fich >> nt; fich.getline(blabla, 80) ;
    fich >> np; fich.getline(blabla, 80) ;

    cout << "total number of domains :   nz = " << nz << endl ;
    cout << "domains inside the star :   nzet1 = " << nzet1 << endl ;
    cout << "number of points in phi :   np = " << np << endl ;
    cout << "number of points in theta : nt = " << nt << endl ;

    int* nr = new int[nz];
    int* nt_tab = new int[nz];
    int* np_tab = new int[nz];
    double* bornes = new double[nz+1];
     
    fich.getline(blabla, 80);
    for (int l=0; l<nz; l++) {
	fich >> nr[l]; 
	fich >> bornes[l]; fich.getline(blabla, 80) ;
	np_tab[l] = np ; 
	nt_tab[l] = nt ; 
    }
    bornes[nz] = __infinity ; 

    if( nr[nzet1-1] != nr[nzet1] ){
      cout << endl
           << " ERROR! nr[nzet1] != nr[nzet1+1]." << endl ;
      cout << " nr[nzet1] = " << nr[nzet1-1] << ", nr[nzet+1] = " << nr[nzet1]
           << endl ;
      cout << " The numbers of points in r"
           << " for the two domains that touch at the surface of the star"
           << " must be the same, since the fields are smoothed out at the "
           << " surface of the star! Please set them to the same value"
           << " in the file par_grid1.d . Aborting..."
           << endl ;
      abort() ;
    }

    // DEBUGGING

    for (int l=0; l<nz; l++) {
      cout << nr[l] << endl ;
      cout << bornes[l] << endl ;
      cout << endl ;
    }
    cout << endl ;

    // END DEBUGGING

    Tbl ent_limit1(nzet1) ;
    Tbl* pent_limit1 ; 
    ent_limit1.set_etat_qcq() ;
    ent_limit1.set(nzet1-1) = 0 ;         // enthalpy at the stellar surface                  
    fich >> ent_limit1.set(0) ;
    //ent_limit1.set(0) = ent_c1/1.8 ;
    //ent_limit1.set(1) = 0. ;
    //cout << "ent_limit:" << endl;
    if ( fich.good() ) {		// to ensure backwards compatibility
      for (int l=1; l<nzet1-1; l++) {
        fich >> ent_limit1.set(l) ; fich.getline(blabla, 120) ;
        //cout << ent_limit1(l) << endl ;
      }
      pent_limit1 = &ent_limit1 ;
    }
    else {
      pent_limit1 = 0x0 ;
    }
    //cout << "end of ent_limit:" << endl;

    fich.close();

    //for (int l=0; l<nzet1-1; l++) {
    //
    //  bornes[l+1] = bornes[nzet1] * sqrt(1. - ent_limit1(l) / ent_c1) ;
    //
    //}

    // DEBUGGING

    for (int l=0; l<nz; l++) {
      cout << nr[l] << endl ;
      cout << bornes[l] << endl ;
      cout << ent_limit1(0) << endl ;
      cout << endl ;
    }
    cout << endl ;

    //abort() ;

    // END DEBUGGING

    // Type of r sampling : 
    int* type_r = new int[nz];
    type_r[0] = RARE ;
    for (int l=1; l<nz-1; l++) {
      type_r[l] = FIN ; 
    }
    type_r[nz-1] = UNSURR ; 
    
    // Type of sampling in theta and phi :
    int type_t = SYM ; 
    int type_p = NONSYM ; 
    
    //-----------------------------------------------------------------------
    //		Construction of multi-grid 1 and mapping 1
    //-----------------------------------------------------------------------
   
    Mg3d mg1(nz, nr, type_r, nt_tab, type_t, np_tab, type_p) ;

    Map_et mp1(mg1, bornes) ;

    // DEBUGGING

    //cout << "Printing alpha and beta for Map_et mp1:" << endl ;

    //for( int l = 0; l < nz; l++ ) {
    //  cout << mp1.get_alpha()[l] << endl ;
    //  cout << mp1.get_beta()[l]  << endl ;
    //}

    // END DEBUGGING
   
    delete [] nr ; 
    delete [] nt_tab ; 
    delete [] np_tab ; 
    delete [] type_r ; 
    delete [] bornes ; 
       
    //-----------------------------------------------------------------------
    //		Input data for the multi-grid no. 2
    //-----------------------------------------------------------------------


    fich.open("par_grid2.d") ;
    fich.getline(blabla, 80);
    fich.getline(blabla, 80);
    fich >> nz; fich.getline(blabla, 80) ;
    int nzet2 ; 
    fich >> nzet2; fich.getline(blabla, 80) ;
    fich >> nt; fich.getline(blabla, 80) ;
    fich >> np; fich.getline(blabla, 80) ;

    cout << "total number of domains :   nz = " << nz << endl ;
    cout << "domains inside the star :   nzet2 = " << nzet2 << endl ;
    cout << "number of points in phi :   np = " << np << endl ;
    cout << "number of points in theta : nt = " << nt << endl ;

    nr = new int[nz];
    nt_tab = new int[nz];
    np_tab = new int[nz];
    bornes = new double[nz+1];
     
    fich.getline(blabla, 80);
    for (int l=0; l<nz; l++) {
	fich >> nr[l]; 
	fich >> bornes[l]; fich.getline(blabla, 80) ;
	np_tab[l] = np ; 
	nt_tab[l] = nt ; 
    }
    bornes[nz] = __infinity ; 

    if( nr[nzet2-1] != nr[nzet2] ){
      cout << endl
           << " ERROR! nr[nzet2-1] != nr[nzet2]." << endl ;
      cout << " nr[nzet1] = " << nr[nzet2-1] << ", nr[nzet] = " << nr[nzet2]
           << endl ;
      cout << " The numbers of points in r"
           << " for the two domains that touch at the surface of the star"
           << " must be the same, since the fields are smoothed out at the "
           << " surface of the star! Please set them to the same value"
           << " in the file par_grid2.d . Aborting..."
           << endl ;
      abort() ;
    }

    Tbl ent_limit2(nzet2) ;
    Tbl* pent_limit2 ;
    ent_limit2.set_etat_qcq() ;
    ent_limit2.set(nzet2-1) = 0 ;         // enthalpy at the stellar surface                  
    fich >> ent_limit2.set(0) ;
    if ( fich.good() ) {	// to ensure backwards compatibility
      for (int l=1; l<nzet2-1; l++) {
	fich >> ent_limit2.set(l) ; fich.getline(blabla, 120) ;
      }
      pent_limit2 = &ent_limit2 ; 
    }
    else {
      pent_limit2 = 0x0 ; 
    }


    for (int l=0; l<nzet2-1; l++) {
      fich >> ent_limit2.set(l) ; fich.getline(blabla, 120) ; 
    }

    fich.close();

    //for (int l=0; l<nzet2-1; l++) {
    //
    //  bornes[l+1] = bornes[nzet2] * sqrt(1 - ent_limit2(l) / ent_c2) ;
    //
    //}

    // Type of r sampling :                            
    type_r = new int[nz];
    type_r[0] = RARE ;
    for (int l=1; l<nz-1; l++) {
      type_r[l] = FIN ;
    }
    type_r[nz-1] = UNSURR ;

    // Type of sampling in theta and phi :             
    type_t = SYM ;
    type_p = NONSYM ;

    //-----------------------------------------------------------------------
    //		Construction of multi-grid 2 and mapping 2
    //-----------------------------------------------------------------------
   
    Mg3d mg2(nz, nr, type_r, nt_tab, type_t, np_tab, type_p) ;

    Map_et mp2(mg2, bornes) ;
   
    delete [] nr ; 
    delete [] nt_tab ; 
    delete [] np_tab ; 
    delete [] type_r ; 
    delete [] bornes ; 
       
    cout << endl << "Multi-grid 1 : " 
	 << endl << "============   " << endl << mg1 << endl ; 
    cout << "Mapping 1 : " 
	 << endl << "=========   " << endl << mp1 << endl ; 

    cout << endl << "Multi-grid 2 : " 
	 << endl << "============   " << endl << mg2 << endl ; 
    cout << "Mapping 2 : " 
	 << endl << "=========   " << endl << mp2 << endl ; 

    //-----------------------------------------------------------------------
    //		Equation of state for star 1
    //-----------------------------------------------------------------------

    fich.open("par_eos1.d") ;

    Eos* peos1 = Eos::eos_from_file(fich) ; 
    Eos& eos1 = *peos1 ; 

    fich.close() ; 

    //-----------------------------------------------------------------------
    //		Equation of state for star 2
    //-----------------------------------------------------------------------

    fich.open("par_eos2.d") ;

    Eos* peos2 = Eos::eos_from_file(fich) ; 
    Eos& eos2 = *peos2 ; 

    fich.close() ; 

    cout << endl << "Equation of state of star 1 : " 
	 << endl << "===========================   " << endl << eos1 << endl ; 
    cout << endl << "Equation of state of star 2 : " 
	 << endl << "===========================   " << endl << eos2 << endl ; 


    //-----------------------------------------------------------------------
    //		Construction of a binary system
    //-----------------------------------------------------------------------
    
    Binaire star(mp1, nzet1, eos1, irrot1, 
		 mp2, nzet2, eos2, irrot2,
		 relat) ;

    //-----------------------------------------------------------------------
    //		START debugging
    //-----------------------------------------------------------------------

      cout << endl << "** Debugging..." << endl << endl ;

      double ent= 0 ;
      double press_cgs= 0 ;
      double nbar_cgs= 0 ;
      double ener_cgs= 0 ;
      double spec_energy= 0 ;
      double min_ent = 0 ;
      double max_ent = 1.255 ; // 5
      int n_ent = 50000 ;
      double d_ent = std::abs(max_ent-min_ent)/(n_ent - 1) ;

      ofstream eos_nosol("eos_nosol.dat") ;

      if( eos_nosol ){

        eos_nosol << setw(15) << "# log10(specific_enthalpy)" << " "
                  << setw(15) << "baryon mass density [g cm^{-3}]" << " "
                  << setw(15) << "pressure [dyne cm^{-2}]" << " "
                  << setw(15) << "energy density [erg cm^{-3}]" << " "
                  << setw(15) << "specific internal energy [c^2]" << " "
                  << endl ;

        for( int i = 0; i < n_ent; ++i ){

          ent      = min_ent + i*d_ent ;
          press_cgs= rho_unit * eos1.press_ent_p( ent )*pow(c_si,2)*10.0 ;
          nbar_cgs = rho_unit * eos1.nbar_ent_p( ent )/1000.0 ;
          ener_cgs = rho_unit * eos1.ener_ent_p( ent )*pow(c_si,2)*10.0 ;
          spec_energy= eos1.ener_ent_p( ent )/eos1.nbar_ent_p( ent ) - 1.0 ;

          if( ent > 0 && nbar_cgs > 0 && press_cgs > 0 ){

            eos_nosol << setw(15) << ent << " "
                      << setw(15) << nbar_cgs << " "
                      << setw(15) << press_cgs << " "
                      << setw(15) << ener_cgs << " "
                      << setw(15) << spec_energy << " "
                      << endl ;

          }

        }

      }

      eos_nosol.close() ;

  //  int eos1_id ;
  //  eos1_id = (*peos1).identify() ;
  //  if( eos1_id == 110 ) { // Piecewise polytrope for star 1
  //
  //    const Eos_multi_poly* p_eos1 =
  //                            dynamic_cast<const Eos_multi_poly*>( peos1 ) ;
  //
  //    cout << "From EOS for star 1..." << endl ;
  //    cout << "npeos()  = " << p_eos1->get_npeos()   << endl ;
  //    cout << "gamma(0) = " << p_eos1->get_gamma(0)  << endl ;
  //    cout << "gamma(1) = " << p_eos1->get_gamma(1)  << endl ;
  //    cout << "gamma(2) = " << p_eos1->get_gamma(2)  << endl ;
  //    cout << "gamma(3) = " << p_eos1->get_gamma(3)  << endl ;
  //    cout << "kappa(0) = " << p_eos1->get_kappa(0)  << endl ;
  //    cout << "kappa(1) = " << p_eos1->get_kappa(1)  << endl ;
  //    cout << "kappa(2) = " << p_eos1->get_kappa(2)  << endl ;
  //    cout << "kappa(3) = " << p_eos1->get_kappa(3)  << endl ;
  //    cout << "logP1()  = " << p_eos1->get_logP1()   << endl ;
  //    cout << "logRho(0)= " << p_eos1->get_logRho(0) << endl ;
  //    cout << "logRho(1)= " << p_eos1->get_logRho(1) << endl ;
  //    cout << "logRho(2)= " << p_eos1->get_logRho(2) << endl ;
  //    cout << endl ;
  //
  //  }
  //
  //  int eos2_id ;
  //  eos2_id = (*peos2).identify() ;
  //  if( eos2_id == 110 ) { // Piecewise polytrope for star 2
  //
  //    const Eos_multi_poly* p_eos2 =
  //                            dynamic_cast<const Eos_multi_poly*>( peos2 ) ;
  //
  //    cout << "From EOS for star 2..." << endl ;
  //    cout << "npeos()  = " << p_eos2->get_npeos()   << endl ;
  //    cout << "gamma(0) = " << p_eos2->get_gamma(0)  << endl ;
  //    cout << "gamma(1) = " << p_eos2->get_gamma(1)  << endl ;
  //    cout << "gamma(2) = " << p_eos2->get_gamma(2)  << endl ;
  //    cout << "gamma(3) = " << p_eos2->get_gamma(3)  << endl ;
  //    cout << "kappa(0) = " << p_eos2->get_kappa(0)  << endl ;
  //    cout << "kappa(1) = " << p_eos2->get_kappa(1)  << endl ;
  //    cout << "kappa(2) = " << p_eos2->get_kappa(2)  << endl ;
  //    cout << "kappa(3) = " << p_eos2->get_kappa(3)  << endl ;
  //    cout << "logP1()  = " << p_eos2->get_logP1()   << endl ;
  //    cout << "logRho(0)= " << p_eos2->get_logRho(0) << endl ;
  //    cout << "logRho(1)= " << p_eos2->get_logRho(1) << endl ;
  //    cout << "logRho(2)= " << p_eos2->get_logRho(2) << endl ;
  //    cout << endl ;
  //
  //  }

  //====================================================

  //  ofstream eos_nosol("eos_nosol2.dat") ;
  //
  //  if( eos_nosol ){
  //
  //    eos_nosol << setw(15) << "log10(specific_enthalpy)" << " "
  //              << setw(15) << "baryon mass density [g cm^{-3}]" << " "
  //              << setw(15) << "pressure [dyne cm^{-2}]" << " "
  //              << setw(15) << "energy density [erg cm^{-3}]" << " "
  //              << setw(15) << "specific energy (or internal energy) [c^2]" << " "
  //              << endl ;
  //
  //    Cmp ent_eos = ent() ; // This is the enthalpy field that we may use to
  //                          // compute the other variables in SPHINCS
  //
  //    for(int l=0; l<nzet; l++) {
  //
  //
  //      Param par ;     // Paramater for multi-domain equation of state
  //      par.add_int(l) ;
  //
  //      press.set_etat_qcq() ;
  //      press.set() = 0 ;
  //
  //      press = press() + eos.press_ent(ent_eos, 1, l, &par) ;
  //
  //      if( ent > 0 && nbar_cgs > 0 && press_cgs > 0 ){
  //
  //        eos_nosol << setw(15) << ent << " "
  //                  << setw(15) << nbar_cgs << " "
  //                  << setw(15) << press_cgs << " "
  //                  << setw(15) << ener_cgs << " "
  //                  << setw(15) << spec_energy << " "
  //                  << endl ;
  //
  //      }
  //
  //    }
  //
  //  }
  //
  //  eos_nosol.close() ;

    //cout << endl << "** End of debugging..." << endl ;

    //-----------------------------------------------------------------------
    //		END debugging
    //-----------------------------------------------------------------------
    
    //-----------------------------------------------------------------------
    //		Computation of two static configurations
    //-----------------------------------------------------------------------

    double precis = 5.e-14 ;
    
    cout << endl << "Computation of a static configuration for star 1"
	 << endl << "================================================" << endl ; 

    (star.set(1)).equilibrium_spher(ent_c1, precis, pent_limit1) ; 

    //abort() ;

    cout << endl << "Computation of a static configuration for star 2"
	 << endl << "================================================" << endl ; 

    (star.set(2)).equilibrium_spher(ent_c2, precis, pent_limit2) ; 


    //-----------------------------------------------------------------------
    //		Sets the stars at Newtonian (Keplerian) position 
    //-----------------------------------------------------------------------

    // Omega and rotation axis
    // -----------------------

    double total_mass =  star(1).mass_g() + star(2).mass_g() ; 

    star.set_omega() = sqrt( g_si/g_unit * total_mass / pow(separ, 3.) ) ;
 
    star.set_x_axe() = 0 ; 


    // Position of the two stars
    // -------------------------
    
    for (int i=1 ; i<=2 ; i++) {

	double xa_et = (star(3-i).mass_g()) / total_mass * separ ;
	if (i == 1) xa_et = - xa_et ; 

	((star.set(i)).set_mp()).set_ori(xa_et, 0., 0.) ; 	
    }

    // Orientation of the two stars
    // ----------------------------
    
    // Star 1 aligned with the absolute frame : 
    ((star.set(1)).set_mp()).set_rot_phi(0.) ; 
    
    // Star 2 anti-aligned with the absolute frame : 
    ((star.set(2)).set_mp()).set_rot_phi(M_PI) ; 
    
        
    cout << endl 
    << "=============================================================" << endl 
    << "=============================================================" << endl ;
    cout << endl << "Final characteristics of the computed system : " << endl ; 
    cout.precision(16) ; 
    cout << star << endl ; 
    
    //-----------------------------------------------------------------------
    //		The result is written in a file
    //-----------------------------------------------------------------------

    FILE* fresu = fopen("ini.d", "w") ; 
    
    int mer = 0 ; 
    fwrite(&mer, sizeof(int), 1, fresu) ;	// mer

    mg1.sauve(fresu) ; 
    mp1.sauve(fresu) ; 
    eos1.sauve(fresu) ; 

    mg2.sauve(fresu) ; 
    mp2.sauve(fresu) ; 
    eos2.sauve(fresu) ; 

    star.sauve(fresu) ;     

    fclose(fresu) ;     

    // Cleaning
    // --------

    delete peos1 ;    
    delete peos2 ;    

    //-----------------------------------------------------------------------
    //		DEBUGGING
    //-----------------------------------------------------------------------

    cout << endl << "** Debugging..." << endl ;

    double rad1_x_comp, rad1_y, rad1_z, rad1_x_opp, center1,
           rad2_x_comp, rad2_y, rad2_z, rad2_x_opp, center2 ;

    rad1_x_comp = star(1).ray_eq() / km ;
    rad1_y = star(1).ray_eq_pis2() / km ;
    rad1_z = star(1).ray_pole() / km ;
    rad1_x_opp = star(1).ray_eq_pi() / km ;
    center1 = mp1.get_ori_x() / km ;
    rad2_x_comp = star(2).ray_eq() / km ;
    rad2_y = star(2).ray_eq_pis2() / km ;
    rad2_z = star(2).ray_pole() / km ;
    rad2_x_opp = star(2).ray_eq_pi() / km ;
    center2 = mp2.get_ori_x() / km ;

    int l1 ; double xi1 ; double theta1 ; double phi1 ;
    int l2 ; double xi2 ; double theta2 ; double phi2 ;

    double x, y, z ;
    double baryon_density, energy_density, pressure, specific_energy,
           log_specific_enthalpy ;//, press1, press2;
    //double ux, uy, uz ;

    double min_x = center1 - std::max(rad1_x_comp,rad1_x_opp) ;
    double max_x = center1 + std::max(rad1_x_comp,rad1_x_opp) ;
    double min_y = - rad1_y ;
    //double max_y = rad1_y ;
    double min_z = - rad1_z ;
    //double max_z = rad1_z ;
    int nnx = 25 ;
    int nny = 25 ;
    int nnz = 25 ;
    double ddx = std::abs(max_x-min_x)/(nnx - 1) ;
    double ddy = 2*rad1_y/(nny - 1)  ;
    double ddz = 2*rad1_z/(nnz - 1)  ;

    Valeur vnbar1 = (star(1).get_nbar()()).va ;
    Valeur vnbar2 = (star(2).get_nbar()()).va ;
    vnbar1.coef() ;
    vnbar2.coef() ;

    Valeur vener1 = (star(1).get_ener()()).va ;
    Valeur vener2 = (star(2).get_ener()()).va ;
    vener1.coef() ;
    vener2.coef() ;

    Valeur vpress1 = (star(1).get_press()()).va ;
    Valeur vpress2 = (star(2).get_press()()).va ;
    vpress1.coef() ;
    vpress2.coef() ;

    Valeur vent1 = (star(1).get_ent()()).va ;
    Valeur vent2 = (star(2).get_ent()()).va ;
    vent1.coef() ;
    vent2.coef() ;

    //cout << " * Before loop for star 1" << endl ;

    ofstream eos_lorene("eos_lorene_tovstar1.dat") ;

    if( eos_lorene ){

      for( int i = 0; i < nnx; ++i ){

        x = min_x + i*ddx ;

        for( int j = 0; j < nny; ++j ){

          y = min_y + j*ddy ;

          for( int k = 0; k < nnz; ++k ){

            z = min_z + k*ddz ;

            //cout << " * Before computing coordinates" << endl ;

            // Values of (l1, xi1, theta1, phi1) (grid 1)
            // corresponding to (x,y,z):
            double r1 ;   // polar coordinates centered on NS 1
            mp1.convert_absolute(x*km, y*km, z*km, r1, theta1, phi1) ;
            mp1.val_lx(r1, theta1, phi1, l1, xi1) ;

            // Values of (l2, xi2, theta2, phi2) (grid 2)
            // corresponding to (x,y,z):
            double r2 ;   // polar coordinates centered on NS 1
            mp2.convert_absolute(x*km, y*km, z*km, r2, theta2, phi2) ;
            mp2.val_lx(r2, theta2, phi2, l2, xi2) ;

            //cout << " * Before computing baryon density" << endl ;

            baryon_density = rho_unit * (
                  vnbar1.c_cf->val_point_symy(l1, xi1, theta1, phi1)
                + vnbar2.c_cf->val_point_symy(l2, xi2, theta2, phi2)
                ) ;

            //cout << " * Before computing energy density" << endl ;

            energy_density =
              rho_unit * ( vener1.c_cf->val_point_symy(l1, xi1, theta1, phi1)
                         + vener2.c_cf->val_point_symy(l2, xi2, theta2, phi2)
                                          ) ;

            //cout << " * Before computing pressure" << endl ;

            pressure = rho_unit *
                ( vpress1.c_cf->val_point_symy(l1, xi1, theta1, phi1)
                + vpress2.c_cf->val_point_symy(l2, xi2, theta2, phi2)
                      ) ;

            //cout << " * Before computing specific energy" << endl ;

            if ( baryon_density == double(0) ) {
              specific_energy = 0 ;
            }
            else {
              specific_energy = energy_density/baryon_density - double(1) ;
            }

            //cout << " * Before computing log of specific enthalpy" << endl ;

            log_specific_enthalpy =
                        ( vent1.c_cf->val_point_symy(l1, xi1, theta1, phi1)
                        + vent2.c_cf->val_point_symy(l2, xi2, theta2, phi2)
                                        ) ;

            // v_x, v_y, v_z (spatial part of the fluid velocity
            // wrt the Eulerian observer)
            //ux = get_ueuler_x( l1, xi1, theta1, phi1,
            //                            l2, xi2, theta2, phi2 ) ;
            //uy = get_ueuler_y( l1, xi1, theta1, phi1,
            //                            l2, xi2, theta2, phi2 ) ;
            //uz = get_ueuler_z( l1, xi1, theta1, phi1,
            //                            l2, xi2, theta2, phi2 ) ;

      //    }
      //  }
      //}
      //
      //for( int i = 0; i < nnx; ++i ){

            //cout << " * Before printing to file" << endl ;

            eos_lorene << setw(15) << "# x[km] "
                       << setw(15) << "y[km] "
                       << setw(15) << "z[km] "
                       << setw(15) << "baryon density [kg/m^3] "
                       << setw(15) << "energy density [kg c^2/m^3] "
                       << setw(15) << "pressure [kg c^2/m^3] "
                       << setw(15) << "specific internal energy [c^2] "
                       << setw(15) << "base-10 log of specific enthalpy "
                       << endl ;

            if( baryon_density > 0 && energy_density > 0
                && pressure > 0 && specific_energy > 0
                && log_specific_enthalpy > 0 ){

              eos_lorene << setw(15) << x << " "
                         << setw(15) << y << " "
                         << setw(15) << z << " "
                         << setw(15) << baryon_density << " "
                         << setw(15) << energy_density << " "
                         << setw(15) << pressure << " "
                         << setw(15) << specific_energy << " "
                         << setw(15) << log_specific_enthalpy
                         << endl ;

            }

          }
        }
      }
    }

    eos_lorene.close() ;

    //cout << " * After loop for star 1" << endl ;

    min_x = center2 - std::max(rad2_x_comp,rad2_x_opp) ;
    max_x = center2 + std::max(rad2_x_comp,rad2_x_opp) ;
    min_y = - rad2_y ;
    //max_y = rad1_y ;
    min_z = - rad2_z ;
    //max_z = rad1_z ;
    nnx = 25 ;
    nny = 25 ;
    nnz = 25 ;
    ddx = std::abs(max_x-min_x)/(nnx - 1) ;
    ddy = 2*rad2_y/(nny - 1)  ;
    ddz = 2*rad2_z/(nnz - 1)  ;

    //cout << " * Before loop for star 2" << endl ;

    ofstream eos_lorene2("eos_lorene_tovstar2.dat") ;

    if( eos_lorene2 ){

      for( int i = 0; i < nnx; ++i ){

        x = min_x + i*ddx ;

        for( int j = 0; j < nny; ++j ){

          y = min_y + j*ddy ;

          for( int k = 0; k < nnz; ++k ){

            z = min_z + k*ddz ;

            // Values of (l1, xi1, theta1, phi1) (grid 1)
            // corresponding to (x,y,z):
            double r1 ;   // polar coordinates centered on NS 1
            mp1.convert_absolute(x*km, y*km, z*km, r1, theta1, phi1) ;
            mp1.val_lx(r1, theta1, phi1, l1, xi1) ;

            // Values of (l2, xi2, theta2, phi2) (grid 2)
            // corresponding to (x,y,z):
            double r2 ;   // polar coordinates centered on NS 1
            mp2.convert_absolute(x*km, y*km, z*km, r2, theta2, phi2) ;
            mp2.val_lx(r2, theta2, phi2, l2, xi2) ;

            baryon_density = rho_unit * (
                  vnbar1.c_cf->val_point_symy(l1, xi1, theta1, phi1)
                + vnbar2.c_cf->val_point_symy(l2, xi2, theta2, phi2)
                ) ;

            energy_density =
              rho_unit * ( vener1.c_cf->val_point_symy(l1, xi1, theta1, phi1)
                         + vener2.c_cf->val_point_symy(l2, xi2, theta2, phi2)
                                          ) ;

            pressure = rho_unit *
                ( vpress1.c_cf->val_point_symy(l1, xi1, theta1, phi1)
                + vpress2.c_cf->val_point_symy(l2, xi2, theta2, phi2)
                      ) ;

            if ( baryon_density == double(0) ) {
              specific_energy = 0 ;
            }
            else {
              specific_energy = energy_density
                            / (baryon_density/rho_unit) - double(1) ;
            }

            log_specific_enthalpy =
                        ( vent1.c_cf-> val_point_symy(l1, xi1, theta1, phi1)
                        + vent2.c_cf-> val_point_symy(l2, xi2, theta2, phi2)
                                        ) ;

            // v_x, v_y, v_z (spatial part of the fluid velocity
            // wrt the Eulerian observer)
            //ux = get_ueuler_x( l1, xi1, theta1, phi1,
            //                            l2, xi2, theta2, phi2 ) ;
            //uy = get_ueuler_y( l1, xi1, theta1, phi1,
            //                            l2, xi2, theta2, phi2 ) ;
            //uz = get_ueuler_z( l1, xi1, theta1, phi1,
            //                            l2, xi2, theta2, phi2 ) ;

      //    }
      //  }
      //}
      //
      //for( int i = 0; i < nnx; ++i ){

            eos_lorene2 << setw(15) << "# x[km] "
                        << setw(15) << "y[km] "
                        << setw(15) << "z[km] "
                        << setw(15) << "baryon density [kg/m^3] "
                        << setw(15) << "energy density [kg c^2/m^3] "
                        << setw(15) << "pressure [kg c^2/m^3] "
                        << setw(15) << "specific internal energy [c^2] "
                        << setw(15) << "base-10 log of specific enthalpy "
                        << endl ;

            if( baryon_density > 0 && energy_density > 0
                && pressure > 0 && specific_energy > 0
                && log_specific_enthalpy > 0 ){

              eos_lorene2 << setw(15) << x << " "
                          << setw(15) << y << " "
                          << setw(15) << z << " "
                          << setw(15) << baryon_density << " "
                          << setw(15) << energy_density << " "
                          << setw(15) << pressure << " "
                          << setw(15) << specific_energy << " "
                          << setw(15) << log_specific_enthalpy
                          << endl ;

            }

          }
        }
      }
    }

    eos_lorene2.close() ;

    cout << " * End of debugging." << endl ;

    return EXIT_SUCCESS ; 
    
    
}
