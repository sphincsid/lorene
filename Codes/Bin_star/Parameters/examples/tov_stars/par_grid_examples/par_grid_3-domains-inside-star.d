# Multi-grid parameters
#######################
7	nz: total number of domains
3	nzet: number of domains inside the star
33	nt: number of points in theta (the same in each domain)
32	np: number of points in phi   (the same in each domain)
# Number of points in r and (initial) inner boundary of each domain:
73	0.	<-   nr	  &   min(r)  in domain 0  (nucleus)
73	0.85	<-   nr	  &   min(r)  in domain 1
109	0.93	<-   nr	  &   min(r)  in domain 2
109	1.	<-   nr	  &   min(r)  in domain 3
41	1.5	<-   nr	  &   min(r)  in domain 4
41	2.5	<-   nr   &   min(r)  in domain 5
41	3.5	<-   nr   &   min(r)  in domain 6








