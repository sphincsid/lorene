# Multi-grid parameters
#######################
5	nz: total number of domains
1	nzet: number of domains inside the star
33	nt: number of points in theta (the same in each domain)
32	np: number of points in phi   (the same in each domain)
# Number of points in r and (initial) inner boundary of each domain:
73	0.	<-   nr	  &   min(r)  in domain 0  (nucleus)
73	1	<-   nr	  &   min(r)  in domain 1
41	1.5	<-   nr	  &   min(r)  in domain 2
41	2.5	<-   nr   &   min(r)  in domain 3
41	3.5	<-   nr   &   min(r)  in domain 4








