110        Type of the EOS (cf. documentation of Eos::eos_multi_poly)
Multipolytropic APR4 EOS
4                 npoly,
1.356923950       gamma_0,
2.830000000       gamma_1,
3.445000000       gamma_2
3.348000000       gamma_3
3.997200658e-008  kappa0,
11.843413106      log10(P0/c^2),
14.180350458      log10(rho_0),
14.700467743      log10(rho_1)
15.000468167      log10(rho_2)
0.0               decInc,
0.0
0.0
