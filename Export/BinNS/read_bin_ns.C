/*
 * Reads binary neutron star initial data, exporting Lorene structures
 * onto standard C arrays (double[]) on a Cartesian grid.
 */

/*
 *   Copyright (c) 2020, 2022  Francesco Torsello
 *
 *   Copyright (c) 2002  Eric Gourgoulhon
 *   Copyright (c) 2002  Keisuke Taniguchi
 *
 *   This file is part of LORENE.
 *
 *   LORENE is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2
 *   as published by the Free Software Foundation.
 *
 *   LORENE is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with LORENE; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */



/*
 * $Id: read_bin_ns.C,v 1.6 2016/12/05 16:18:30 j_novak Exp $
 * $Log: read_bin_ns.C,v $
 * Revision 1.6  2016/12/05 16:18:30  j_novak
 * Suppression of some global variables (file names, loch, ...) to prevent redefinitions
 *
 * Revision 1.5  2014/10/13 08:54:05  j_novak
 * Lorene classes and functions now belong to the namespace Lorene.
 *
 * Revision 1.4  2014/10/06 15:09:47  j_novak
 * Modified #include directives to use c++ syntax.
 *
 * Revision 1.3  2003/01/09 11:08:00  j_novak
 * headcpp.h is now compliant with C++ norm.
 * The include files have been ordered, as well as the local_settings_linux
 *
 * Revision 1.2  2002/01/15 09:35:33  e_gourgoulhon
 * Added README file
 * Suppressed outputs/inputs in read_bin_ns.C
 *
 * Revision 1.1  2002/01/11 17:16:14  e_gourgoulhon
 * Main code and documentation for exporting binary neutron stars
 *
 *
 * $Header: /cvsroot/Lorene/Export/BinNS/read_bin_ns.C,v 1.6 2016/12/05 16:18:30 j_novak Exp $
 *
 */

// C headers
#include <cstdlib>

// Lorene headers
//#include "../../C++/Include/tenseur.h"
//#include "../../C++/Include/binaire.h"
//#include "../../C++/Include/eos.h"
//#include "../../C++/Include/unites.h"

// Definition of Bin_NS class
#include "bin_ns.h"

using namespace Lorene ;
using namespace Unites ;

int main() {

    // Read of the initial data file and computation of the
    // fields at the Cartesian grid points
    // ----------------------------------------------------


    //Bin_NS binary(nbp, xi, yi, zi, datafile) ;

    //cout << endl << binary << endl ;



    // Create a new object from a binary file
    //---------------------------------------
    // finib = fopen("inib.d", "r") ;
    // Bin_NS binary2(finib) ;
    // fclose( finib ) ;

    // cout << endl << "Binary read in the binary file : " << binary2 << endl ;

    // finib = fopen("inib2.d", "w") ;
    // binary2.save_bin(finib) ;
    // fclose( finib ) ;

    // Create a new object from a formatted file
    //---------------------------------------
    // ifstream finif3("inif.d") ;
    // Bin_NS binary3(finif3) ;
    // finif3.close() ;

    // cout << endl << "Binary read in the formatted file : "
    //      << binary3 << endl ;

    // finif.open("inif3.d") ;
    // binary3.save_form(finif) ;
    // finif.close() ;

    //Bin_NS binary(file_points, datafile) ;

    Bin_NS binary ;

    cout << "angular_vel= "      << binary.omega       << endl ;
    cout << "distance= "         << binary.dist        << endl ;
    cout << "distance_com= "     << binary.dist_mass   << endl ;
    cout << "mass1= "            << binary.mass1_b     << endl ;
    cout << "mass2= "            << binary.mass2_b     << endl ;
    cout << "adm_mass= "         << binary.mass_adm    << endl ;
    //cout << "angular_momentum_x= " << binary.angu_mom_x    << endl ;
    cout << "radius1_x_comp= "   << binary.rad1_x_comp << endl ;
    cout << "radius1_y= "        << binary.rad1_y      << endl ;
    cout << "radius1_z= "        << binary.rad1_z      << endl ;
    cout << "radius1_x_opp= "    << binary.rad1_x_opp  << endl ;
    cout << "radius2_x_comp= "   << binary.rad2_x_comp << endl ;
    cout << "radius2_y= "        << binary.rad2_y      << endl ;
    cout << "radius2_z= "        << binary.rad2_z      << endl ;
    cout << "radius2_x_opp= "    << binary.rad2_x_opp  << endl << endl ;

    cout << "eos_name1= "        << binary.eos_name1   << endl ;
    cout << "kappa_poly1= "      << binary.kappa_poly1 << endl ;
    cout << "gamma_poly1= "      << binary.gamma_poly1 << endl ;
    cout << "eos_name2= "        << binary.eos_name2   << endl ;
    cout << "kappa_poly2= "      << binary.kappa_poly2 << endl ;
    cout << "gamma_poly2= "      << binary.gamma_poly2 << endl << endl ;

    double tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, tmp9,
           tmp10, tmp11, tmp12, tmp13, tmp14, tmp15 ;
    char eos1[100] = "eos1" ;
    char eos2[100] = "eos2" ;

    //binary.get_params( tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, tmp9,
    //                   tmp10, tmp11, tmp12, tmp13, tmp14, tmp15, eos1, eos2 ) ;

    cout << "eos1= "        << eos1   << endl ;
    cout << "eos2= "        << eos2   << endl << endl ;

    cout << "npeos_1= "       << binary.npeos_1   << endl ;
    cout << "gamma0_1= "      << binary.gamma0_1  << endl ;
    cout << "gamma1_1= "      << binary.gamma1_1  << endl ;
    cout << "gamma2_1= "      << binary.gamma2_1  << endl ;
    cout << "gamma3_1= "      << binary.gamma3_1  << endl ;
    cout << "kappa0_1= "      << binary.kappa0_1  << endl ;
    cout << "logP1_1= "       << binary.logP1_1   << endl ;
    cout << "logRho0_1= "     << binary.logRho0_1 << endl ;
    cout << "logRho1_1= "     << binary.logRho1_1 << endl ;
    cout << "logRho2_1= "     << binary.logRho2_1 << endl << endl ;

    cout << "npeos_2= "       << binary.npeos_2   << endl ;
    cout << "gamma0_2= "      << binary.gamma0_2  << endl ;
    cout << "gamma1_2= "      << binary.gamma1_2  << endl ;
    cout << "gamma2_2= "      << binary.gamma2_2  << endl ;
    cout << "gamma3_2= "      << binary.gamma3_2  << endl ;
    cout << "kappa0_2= "      << binary.kappa0_2  << endl ;
    cout << "logP1_2= "       << binary.logP1_2   << endl ;
    cout << "logRho0_2= "     << binary.logRho0_2 << endl ;
    cout << "logRho1_2= "     << binary.logRho1_2 << endl ;
    cout << "logRho2_2= "     << binary.logRho2_2 << endl << endl ;


    //cout << endl << binary << endl ;

   //binary.export_to_cartesian( "inib.d", "inif.d" ) ;
   //
   ////binary.export_to_points( binary.points_file_format,
   ////                         "inib_pb.d", "inif_pb.d" ) ;
   //
   //double x = 25 ;
   //double y = 0 ;
   //double z = 0 ;
   //
   //int l1, l2 ;
   //double xi1, theta1, phi1 ;
   //double xi2, theta2, phi2 ;
   //
   //binary.cartesian_to_grids( x, y, z,
   //                           l1, xi1, theta1, phi1,
   //                           l2, xi2, theta2, phi2 ) ;

    //cout << binary.get_energy_density( l1, xi1, theta1, phi1,
    //                                   l2, xi2, theta2, phi2 ) << endl ;
    //cout << binary.nx << endl ;
    //cout << binary.ny << endl ;
    //cout << binary.nz << endl ;
    //cout << binary.np << endl << endl ;
    /*for( int i = 0; i < binary.np; ++i ){
      double lapse_temp = binary.nnn[i] ;
      binary.cartesian_to_grids( binary.xx[i],
                                 binary.yy[i], binary.zz[i],
                                 l1, xi1, theta1, phi1,
                                 l2, xi2, theta2, phi2 ) ;
      double lapse_temp2 = binary.get_lapse( l1, xi1, theta1, phi1,
                                             l2, xi2, theta2, phi2 ) ;
      if( lapse_temp > 1e-5 )
      cout << lapse_temp << ", " << lapse_temp2 << endl ;
    }*/
    /*cout << "export_to_cartesian, get_energy_density" << endl ;
    int counter = 0 ;
    for( int i = 0; i < binary.np; ++i ){
      double rho_temp = binary.nbar[i] ;
      binary.cartesian_to_grids( binary.xx[i],
                                 binary.yy[i], binary.zz[i],
                                 l1, xi1, theta1, phi1,
                                 l2, xi2, theta2, phi2 ) ;
      double rho_temp2 = binary.get_baryon_density( l1, xi1, theta1, phi1,
                                                    l2, xi2, theta2, phi2 ) ;
      binary.cartesian_to_grids( binary.xx[i],
                                 binary.yy[i], binary.zz[i],
                                 l1, xi1, theta1, phi1,
                                 l2, xi2, theta2, phi2 ) ;
      double rho_temp3 = binary.get_baryon_density( l1, xi1, theta1, phi1,
                                                    l2, xi2, theta2, phi2 ) ;
      if( rho_temp > 1e-5 ){
        cout << rho_temp << ", " << rho_temp2 << ", " << rho_temp3 << endl ;
        counter++ ;
      }
    }*/
    /*cout << counter ;
    for( int i = 0; i < binary.np; ++i ){
        cout << binary.xx[i] << ", " << binary.yy[i]
             << ", " << binary.zz[i] << endl ;
    }*/
    /*cout << "g_diag" << endl ;
    for( int i = 0; i < binary.np; ++i ){
      double rho_temp = binary.g_xx[i] ;
      binary.cartesian_to_grids( binary.xx[i],
                                 binary.yy[i], binary.zz[i],
                                 l1, xi1, theta1, phi1,
                                 l2, xi2, theta2, phi2 ) ;
      double rho_temp2 = binary.get_g_diag( l1, xi1, theta1, phi1,
                                                    l2, xi2, theta2, phi2 ) ;
      if( rho_temp > 1e-5 )
      cout << rho_temp << ", " << rho_temp2 << endl ;
    }*/
    /*cout << "get_energy_density:" << endl ;
    for( int i = 0; i < binary.np; ++i ){
      binary.cartesian_to_grids( binary.xx[i], binary.yy[i], binary.zz[i],
                                   &l1, &xi1, &theta1, &phi1,
                                   &l2, &xi2, &theta2, &phi2 ) ;
      double rho_temp = binary.get_energy_density( l1, xi1, theta1, phi1,
                                                   l2, xi2, theta2, phi2 ) ;
      if( rho_temp > 1e-5 )
      cout << rho_temp << endl ;
    }*/

    return EXIT_SUCCESS ;
}
