/*
 *  Definition of class Bin_NS (binary neutron star exportation)
 *
 */

/*
 *   Copyright (c) 2020-2023 Francesco Torsello
 *
 *   Copyright (c) 2002  Eric Gourgoulhon
 *   Copyright (c) 2002  Keisuke Taniguchi
 *
 *   This file is part of LORENE.
 *
 *   LORENE is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2
 *   as published by the Free Software Foundation.
 *
 *   LORENE is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with LORENE; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef __BIN_NS_H_
#define __BIN_NS_H_

/*
 * $Id: bin_ns.h,v 1.7 2014/10/13 08:54:05 j_novak Exp $
 * $Log: bin_ns.h,v $
 * Revision 1.7  2014/10/13 08:54:05  j_novak
 * Lorene classes and functions now belong to the namespace Lorene.
 *
 * Revision 1.6  2014/10/06 15:13:25  j_novak
 * Modified #include directives to use c++ syntax.
 *
 * Revision 1.5  2010/07/14 16:47:30  e_gourgoulhon
 * Corrected error in the documentation for K_xx, K_xy, etc...:
 * the components are the covariant ones, not the contravariant ones.
 *
 * Revision 1.4  2004/10/20 15:01:37  e_gourgoulhon
 * Corrected error in the comments on the shift vector:
 * corotating coordinates -> non rotating coordinates.
 *
 * Revision 1.3  2003/10/24 15:49:03  e_gourgoulhon
 * Updated documentation.
 *
 * Revision 1.2  2003/01/09 11:08:00  j_novak
 * headcpp.h is now compliant with C++ norm.
 * The include files have been ordered, as well as the local_settings_linux
 *
 * Revision 1.1  2002/01/11 17:03:02  e_gourgoulhon
 * Exportation of binary neutron stars configuration to a Cartesian grid
 *
 *
 * $Header: /cvsroot/Lorene/Export/C++/Include/bin_ns.h,v 1.7 2014/10/13 08:54:05 j_novak Exp $
 *
 */

// Headers C
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string.h>

// Lorene headers
#include "../../../C++/Include/tenseur.h"
#include "../../../C++/Include/binaire.h"
#include "../../../C++/Include/eos.h"
#include "../../../C++/Include/eos_multi_poly.h"
#include "../../../C++/Include/unites.h"

using namespace std ;

namespace Lorene {
/**
 * Binary neutron star configuration on a Cartesian grid.
 *
 * A binary black hole system is constructed on a Cartesian grid from
 * data stored in a file resulting from a computation by Taniguchi
 * and Gourgoulhon.
 *
 * Importation of Lorene data is performed by means of the constructor
 * {\tt Bin\_NS::Bin\_NS(int, const double*, const double*, const double*, const char*)}.
 * This constructor takes general arrays for the location of the Cartesian coordinates
 * $(x, y, z)$, i.e. it does not assume that the grid is a uniform one. Note also
 * that these arrays are 1-D, as well as all the metric fields,
 * in order to be use with any ordering of the 3-D storage.
 *
 *  This class is very simple, with all data members being public.
 *  A typical example of use is the following one
 *
 *  \begin{verbatim}
 *	    // Define the Cartesian grid by means of the arrays xg, yg, zg:
 *	    for (int i=0; i<nb_points; i++) {
 *           xg[i] = ...
 *           yg[i] = ...
 *           zg[i] = ...
 *	    }
 *
 *	    // Read the file containing the spectral data and evaluate
 *	    //  all the fields on the Cartesian grid :
 *
 *	    Bin_NS binary_system(nb_points, xg, yg, zg, datafile) ;
 *
 *	    // Extract what you need :
 *
 *	    double* gamma_xx = binary_system.g_xx ; // metric coefficient g_xx
 *
 *	    double* shift_x = binary_system.beta_x ; // x comp. of shift vector
 *
 *	    ...
 *
 *	    // Save everything in an ASCII file :
 *
 *	    ofstream file_ini("ini.d") ;
 *	    binary_system.save_form(file_ini) ;
 *	    file_ini.close() ;
 *
 *  \end{verbatim}
 *
 * @version #$Id: bin_ns.h,v 1.7 2014/10/13 08:54:05 j_novak Exp $#
 */

using namespace Unites ;

class Bin_NS {

    // Data :
    // -----
    public:

        Mg3d* vmg1              ;
        Mg3d* vmg2              ;
        Map_et* vmp1            ;
        Map_et* vmp2            ;
        Valeur* vvnn1           ;
        Valeur* vvnn2           ;
        Valeur* vvshiftx1       ;
        Valeur* vvshiftx2       ;
        Valeur* vvshifty1       ;
        Valeur* vvshifty2       ;
        Valeur* vvshiftz1       ;
        Valeur* vvshiftz2       ;
        Valeur* vvacar1         ;
        Valeur* vvacar2         ;
        Valeur* vvkxx1          ;
        Valeur* vvkxx2          ;
        Valeur* vvkxy1          ;
        Valeur* vvkxy2          ;
        Valeur* vvkxz1          ;
        Valeur* vvkxz2          ;
        Valeur* vvkyy1          ;
        Valeur* vvkyy2          ;
        Valeur* vvkyz1          ;
        Valeur* vvkyz2          ;
        Valeur* vvkzz1          ;
        Valeur* vvkzz2          ;
        Valeur* vvnbar1         ;
        Valeur* vvnbar2         ;
        Valeur* vvener1         ;
        Valeur* vvener2         ;
        Valeur* vvpress1        ;
        Valeur* vvpress2        ;
        Valeur* vvent1          ;
        Valeur* vvent2          ;
        Valeur* vvueulerx1      ;
        Valeur* vvueulerx2      ;
        Valeur* vvueulery1      ;
        Valeur* vvueulery2      ;
        Valeur* vvueulerz1      ;
        Valeur* vvueulerz2      ;
        Valeur* vvgamma_euler1  ;
        Valeur* vvgamma_euler2  ;

        char comment[120] ;
        char datafile[120] ;
        char file_points[120] ;
        char points_file_format[120] ;
        double x_min, x_max, y_min, y_max, z_min, z_max ;

        int allocated = 0 ;

        /// Identifier of the EOS for star 1
        int eos1_id = 0 ;

        /// Identifier of the EOS for star 1
        int eos2_id = 0 ;

        /// Eos name star 1
        char eos_name1[100] ;

        /// Adiabatic index of EOS 1 if it is polytropic (0 otherwise)
        double gamma_poly1 ;

        /**
	 *  Polytropic constant of EOS 1 if it is polytropic (0 otherwise)
	 *  [unit: $\rho_{\rm nuc} c^2 / n_{\rm nuc}^\gamma$]
	 */
        double kappa_poly1 ;

        /// Eos name star 2
        char eos_name2[100] ;

        /// Adiabatic index of EOS 2 if it is polytropic (0 otherwise)
        double gamma_poly2 ;

        /**
	 *  Polytropic constant of EOS 2 if it is polytropic (0 otherwise)
	 *  [unit: $\rho_{\rm nuc} c^2 / n_{\rm nuc}^\gamma$]
	 */
        double kappa_poly2 ;

        /// Parameters for multipolytropic EoS for star 1
        int npeos_1 ;
        double gamma0_1 ;
        double gamma1_1 ;
        double gamma2_1 ;
        double gamma3_1 ;
        double kappa0_1 ;
        double kappa1_1 ;
        double kappa2_1 ;
        double kappa3_1 ;
        double logP1_1 ;
        double logRho0_1 ;
        double logRho1_1 ;
        double logRho2_1 ;

        /// Parameters for multipolytropic EoS for star 1
        int npeos_2 ;
        double gamma0_2 ;
        double gamma1_2 ;
        double gamma2_2 ;
        double gamma3_2 ;
        double kappa0_2 ;
        double kappa1_2 ;
        double kappa2_2 ;
        double kappa3_2 ;
        double logP1_2 ;
        double logRho0_2 ;
        double logRho1_2 ;
        double logRho2_2 ;

        /// Name of the Eos file of tabulated Eos for star 1
        char eos_file_name1[500] ;

        /// Name of the Eos file of tabulated Eos for star 2
        char eos_file_name2[500] ;

        /// Pointers to the Eos table for star 1
        /// CGS pressure, baryon number density and internal energy density
        double* press_tbl1 ;
        double* nb_tbl1 ;
        double* ener_tbl1 ;

        /// Pointers to the Eos table for star 2
        /// CGS pressure, baryon number density and internal energy density
        double* press_tbl2 ;
        double* nb_tbl2 ;
        double* ener_tbl2 ;

	/// Orbital angular velocity [unit: rad/s]
	double omega ;

	/** Distance between the centers (maxiumum density) of the two neutron
	 *  stars [unit: km]
	 */
	double dist ;

	/** Distance between the center of masses of two neutron stars
	 *  [unit: km]
	 */
	double dist_mass ;

	/** Baryon mass of star 1
	 *  [unit: $M_\odot$]
	 */
	double mass1_b ;

	/** Baryon mass of star 2
	 *  [unit: $M_\odot$]
	 */
	double mass2_b ;

	/** Gravitational mass of star 1
	 *  [unit: $M_\odot$]
	 */
	double mass1_g ;

	/** Gravitational mass of star 2
	 *  [unit: $M_\odot$]
	 */
	double mass2_g ;

	/** ADM mass of the binary system
	 *  [unit: $M_\odot$]
	 */
	double mass_adm ;

	/** ADM linear momentum of the binary system wrt x axis
	 *  [unit: ]
	 */
	double adm_lin_mom_x ;

	/** ADM linear momentum of the binary system wrt y axis
	 *  [unit: ]
	 */
	double adm_lin_mom_y ;

	/** ADM linear momentum of the binary system wrt z axis
	 *  [unit: ]
	 */
	double adm_lin_mom_z ;

	/** Total angular momentum of the binary system wrt x axis
	 *  [unit: $GM_\odot^2/c$]
	 */
	double angu_mom_x ;

	/** Total angular momentum of the binary system wrt y axis
	 *  [unit: $GM_\odot^2/c$]
	 */
	double angu_mom_y ;

	/** Total angular momentum of the binary system wrt z axis
	 *  [unit: $GM_\odot^2/c$]
	 */
	double angu_mom_z ;

	/** Areal (or circunferential) radius of star 1
	 *  [unit: km]
	 */
	double area_rad1 ;

	/** Coordinate radius of star 1 (less massive star)
	 *  parallel to the x axis toward the companion star
	 *  [unit: km]
	 */
	double rad1_x_comp ;

	/** Coordinate radius of star 1 (less massive star)
	 *  parallel to the y axis [unit: km].
	 */
	double rad1_y ;

	/** Coordinate radius of star 1 (less massive star)
	 *  parallel to the z axis [unit: km].
	 */
	double rad1_z ;

	/** Coordinate radius of star 1 (less massive star)
	 *  parallel to the x axis opposite to the companion star
	 *  [unit: km].
	 */
	double rad1_x_opp ;

	/** Areal (or circunferential) radius of star 2
	 *  [unit: km]
	 */
	double area_rad2 ;

	/** Coordinate radius of star 2 (massive star)
	 *  parallel to the x axis toward the companion star
	 *  [unit: km].
	 */
	double rad2_x_comp ;

	/** Coordinate radius of star 2 (massive star)
	 *  parallel to the y axis [unit: km].
	 */
	double rad2_y ;

	/** Coordinate radius of star 2 (massive star)
	 *  parallel to the z axis [unit: km].
	 */
	double rad2_z ;

	/** Coordinate radius of star 2 (massive star)
	 *  parallel to the x axis opposite to the companion star
	 *  [unit: km].
	 */
	double rad2_x_opp ;

	/** x coordinate of the stellar center for star 1
	 *  [unit: km].
	 */
	double center1 ;

	/** x coordinate of the stellar barycenter for star 1
	 *  [unit: km].
	 */
	double barycenter1 ;

	/** x coordinate of the stellar center for star 2
	 *  [unit: km].
	 */
	double center2 ;

	/** x coordinate of the stellar barycenter for star 2
	 *  [unit: km].
	 */
	double barycenter2 ;

	/** x coordinate of the barycenter for the binary system
	 *  [unit: km].
	 */
	double barycenter_bns ;

	/// Central enthalpy for star 1 [c^2]
	double ent_center1 ;
	/// Central baryon number density for star 1 [m^-3]
	double nbar_center1 ;
	/// Central baryon mass density for star 1 [kg m^-3]
	double rho_center1 ;
	/// Central energy density for star 1 [kg c^2 m^-3]
	double energy_density_center1 ;
	/// Central specific energy for star 1 [c^2]
	double specific_energy_center1 ;
	/// Central pressure for star 1 [kg c^2 m^-3]
	double pressure_center1 ;

	/// Central enthalpy for star 2 [c^2]
	double ent_center2 ;
	/// Central baryon number density for star 2 [m^-3]
	double nbar_center2 ;
	/// Central baryon mass density for star 2 [kg m^-3]
	double rho_center2 ;
	/// Central energy density for star 2 [kg c^2 m^-3]
	double energy_density_center2 ;
	/// Central specific energy for star 2 [c^2]
	double specific_energy_center2 ;
	/// Central pressure for star 2 [kg c^2 m^-3]
	double pressure_center2 ;

	/// Number of grid points in x
	int nx ;
	/// Number of grid points in y
	int ny ;
	/// Number of grid points in z
	int nz ;
        /// Total number of points
	int np ;

	/// 1-D array storing the values of coordinate x of the {\tt np} grid points [unit: km]
	double* xx ;

	/// 1-D array storing the values of coordinate y of the {\tt np} grid points [unit: km]
	double* yy ;

	/// 1-D array storing the values of coordinate z of the {\tt np} grid points [unit: km]
	double* zz ;

	/// Lapse function $N$ at the {\tt np} grid points (1-D array)
	double* nnn ;

	/// Component $\beta^x$ of the shift vector of non rotating coordinates [unit: $c$]
        double* beta_x ;
	
	/// Component $\beta^y$ of the shift vector of non rotating coordinates [unit: $c$]
	double* beta_y ; 
	
	/// Component $\beta^z$ of the shift vector of non rotating coordinates [unit: $c$]
	double* beta_z ; 
	
	/// Metric coefficient $\gamma_{xx}$ at the grid points (1-D array)
	double* g_xx ; 

	/// Metric coefficient $\gamma_{xy}$ at the grid points (1-D array)
	double* g_xy ;

	/// Metric coefficient $\gamma_{xz}$ at the grid points (1-D array)
	double* g_xz ; 

	/// Metric coefficient $\gamma_{yy}$ at the grid points (1-D array)
	double* g_yy ; 

	/// Metric coefficient $\gamma_{yz}$ at the grid points (1-D array)
	double* g_yz ; 

	/// Metric coefficient $\gamma_{zz}$ at the grid points (1-D array)
	double* g_zz ; 

	/// Component $K_{xx}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
	double* k_xx ;

	/// Component $K_{xy}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
	double* k_xy ;

	/// Component $K_{xz}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
	double* k_xz ;

	/// Component $K_{yy}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
	double* k_yy ;

	/// Component $K_{yz}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
	double* k_yz ;

	/// Component $K_{zz}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
        double* k_zz ;

	// Hydro components
	//------------------
	/** Baryon density in the fluid frame at the {\tt np} grid points (1-D array)
         * [unit: ${\rm kg \, m}^{-3}$]
         */
        double* nbar ;

	/// Specific internal energy at the  {\tt np} grid points (1-D array) [unit: $c^2$]
        double* ener_spec ;

	/** Component $U^x$ of the fluid 3-velocity with respect to the Eulerian
         * observer, at the {\tt np} grid points (1-D array) [unit: $c$]
         */
        double* u_euler_x ;

	/** Component $U^y$ of the fluid 3-velocity with respect to the Eulerian
         * observer, at the {\tt np} grid points (1-D array) [unit: $c$]
         */
        double* u_euler_y ;

	/** Component $U^z$ of the fluid 3-velocity with respect to the Eulerian
         * observer, at the {\tt np} grid points (1-D array) [unit: $c$]
         */
        double* u_euler_z ;


    // Constructors - Destructor
    // -------------------------
    public:
	/** Constructor from Lorene spectral data.
	 *
	 * This constructor takes general arrays {\tt xi, yi, zi}
	 * for the location of the Cartesian coordinates
	 * $(x, y, z)$, i.e. it does not assume that the grid is a uniform one.
	 * These arrays are 1-D to deal with any ordering of a 3-D storage.
	 *
	 *  @param nbpoints [input] Total number of grid points
	 *  @param xi [input] 1-D array (size {\tt nbpoints}) storing the
	 *		values of coordinate x of the grid points [unit: km]
	 *  @param yi [input] 1-D array (size {\tt nbpoints}) storing the
	 *		values of coordinate y of the grid points [unit: km]
	 *  @param zi [input] 1-D array (size {\tt nbpoints}) storing the
	 *		values of coordinate z of the grid points [unit: km]
	 *  @param filename [input] Name of the (binary) file containing the result
	 *		of a computation by means of the multi-domain 
	 *		spectral method.
	 */
	/*Bin_NS(int nbpoints, const double* xi, const double* yi,
		   const double* zi, const char* filename ) ;

	Bin_NS(const char* file_points, const char* filename_spectral ) ;*/

	Bin_NS(char* resu_file = (char*)"read_it",
	       char* eos_file1 = (char*)"use_id",
	       char* eos_file2 = (char*)"use_id") ;


	/** Constructor from a binary file 
	 *   (previously created by {\tt save\_bin})
	 */
	Bin_NS(FILE* ) ;

	/** Constructor from a formatted file
	 *   (previously created by {\tt save\_form})
	 */
	Bin_NS(ifstream& ) ;

	/// Destructor
	~Bin_NS() ;


    // Memory management
    // -----------------
        private:

            /// Allocate the memory for the arrays g\_ij, k\_ij, etc...
            void alloc_memory() ;

        // Outputs
        // -------
        public:
        /** Method to export data on a Cartesian grid
         */
        void export_to_cartesian( const char* name_binary_file,
                                  const char* name_formatted_file ) ;

        /** Method to export data on a list of points
         */
        void export_to_points( const char* file_type,
                               const char* name_binary_file,
                               const char* name_formatted_file ) ;

        /** Methods to access the data at a given point
         *
         */
        void cartesian_to_grids( double  x,  double  y,   double  z,
                                 int&    l1, double& xi1,
                                 double& theta1, double& phi1,
                                 int&    l2, double& xi2,
                                 double& theta2, double& phi2 ) ;

        double get_lapse( int l1, double xi1, double theta1, double phi1,
                          int l2, double xi2, double theta2, double phi2 ) ;

        double get_shift_x( int l1, double xi1, double theta1, double phi1,
                            int l2, double xi2, double theta2, double phi2 ) ;
        double get_shift_y( int l1, double xi1, double theta1, double phi1,
                            int l2, double xi2, double theta2, double phi2 ) ;
        double get_shift_z( int l1, double xi1, double theta1, double phi1,
                            int l2, double xi2, double theta2, double phi2 ) ;

        double get_g_diag( int l1, double xi1, double theta1, double phi1,
                           int l2, double xi2, double theta2, double phi2 ) ;
        double get_g_diag( double x, double y, double z ) ;

        double get_k_xx( int l1, double xi1, double theta1, double phi1,
                         int l2, double xi2, double theta2, double phi2 ) ;
        double get_k_xy( int l1, double xi1, double theta1, double phi1,
                         int l2, double xi2, double theta2, double phi2 ) ;
        double get_k_xz( int l1, double xi1, double theta1, double phi1,
                         int l2, double xi2, double theta2, double phi2 ) ;
        double get_k_yy( int l1, double xi1, double theta1, double phi1,
                         int l2, double xi2, double theta2, double phi2 ) ;
        double get_k_yz( int l1, double xi1, double theta1, double phi1,
                         int l2, double xi2, double theta2, double phi2 ) ;
        double get_k_zz( int l1, double xi1, double theta1, double phi1,
                         int l2, double xi2, double theta2, double phi2 ) ;

        double get_baryon_density(
                        int l1, double xi1, double theta1, double phi1,
                        int l2, double xi2, double theta2, double phi2 ) ;
        double get_baryon_density( double x, double y, double z ) ;

        double get_energy_density(
                        int l1, double xi1, double theta1, double phi1,
                        int l2, double xi2, double theta2, double phi2 ) ;

        double get_pressure(
                        int l1, double xi1, double theta1, double phi1,
                        int l2, double xi2, double theta2, double phi2 ) ;
        double get_pressure( double x, double y, double z ) ;

        double get_log_specific_enthalpy(
                        int l1, double xi1, double theta1, double phi1,
                        int l2, double xi2, double theta2, double phi2 ) ;

        double get_specific_energy(
                        int l1, double xi1, double theta1, double phi1,
                        int l2, double xi2, double theta2, double phi2 ) ;

        int is_hydro_positive( double x, double y, double z ) ;

        double get_ueuler_x( int l1, double xi1, double theta1, double phi1,
                             int l2, double xi2, double theta2, double phi2 ) ;
        double get_ueuler_y( int l1, double xi1, double theta1, double phi1,
                             int l2, double xi2, double theta2, double phi2 ) ;
        double get_ueuler_z( int l1, double xi1, double theta1, double phi1,
                             int l2, double xi2, double theta2, double phi2 ) ;

        void get_fields(double  x,  double  y,   double  z,
                         double& lapse,
                         double& shift_x, double& shift_y, double& shift_z,
                         double& g_diag,
                         double& k_xx, double& k_xy, double& k_xz,
                         double& k_yy, double& k_yz, double& k_zz,
                         double& baryon_density,
                         double& energy_density,
                         double& specific_energy, double &vpressure,
                         double& ueuler_x, double& ueuler_y, double& ueuler_z
                       ) ;

        void get_fields_no_k( double  x,  double  y,   double  z,
                         double& lapse,
                         double& shift_x, double& shift_y, double& shift_z,
                         double& g_diag,
                         double& baryon_density,
                         double& energy_density,
                         double& specific_energy,
                         double& pressure,
                         double& ueuler_x, double& ueuler_y, double& ueuler_z
                       ) ;

        void get_fields_mass_b( double  x,  double  y,   double  z,
                         double& g_diag,
                         double& baryon_density,
                         double& gamma_euler
                       ) ;

        void get_fields_no_hydro( double  x,  double  y,   double  z,
                         double& lapse,
                         double& shift_x, double& shift_y, double& shift_z,
                         double& g_diag,
                         double& k_xx, double& k_xy, double& k_xz,
                         double& k_yy, double& k_yz, double& k_zz
                       ) ;

        void get_fields_hydro( double  x,  double  y,   double  z,
                         double& baryon_density,
                         double& energy_density,
                         double& specific_energy,
                         double& vpressure,
                         double& ueuler_x, double& ueuler_y, double& ueuler_z
                       ) ;

        void print_hydro() ;

        void get_fields_k( double  x,  double  y,   double  z,
                           double& k_xx, double& k_xy, double& k_xz,
                           double& k_yy, double& k_yz, double& k_zz
                       ) ;

        void get_params( double& angular_vel,
                         double& distance,
                         double& distance_com,
                         double& mass1,
                         double& mass2,
                         double& massg1,
                         double& massg2,
                         double& adm_mass,
                         double& linear_momentum_x,
                         double& linear_momentum_y,
                         double& linear_momentum_z,
                         double& angular_momentum_x,
                         double& angular_momentum_y,
                         double& angular_momentum_z,
                         double& area_radius1,
                         double& radius1_x_comp,
                         double& radius1_y,
                         double& radius1_z,
                         double& radius1_x_opp,
                         double& center1_x,
                         double& barycenter1_x,
                         double& area_radius2,
                         double& radius2_x_comp,
                         double& radius2_y,
                         double& radius2_z,
                         double& radius2_x_opp,
                         double& center2_x,
                         double& barycenter2_x,
                         double& ent_center1,
                         double& nbar_center1,
                         double& rho_center1,
                         double& energy_density_center1,
                         double& specific_energy_center1,
                         double& pressure_center1,
                         double& ent_center2,
                         double& nbar_center2,
                         double& rho_center2,
                         double& energy_density_center2,
                         double& specific_energy_center2,
                         double& pressure_center2,
                         char (& eos1)[100],
                         char (& eos2)[100],
                         int& eos1_id,
                         int& eos2_id,
                         double& gamma_1,
                         double& kappa_1,
                         double& gamma_2,
                         double& kappa_2,
                         int& npeos_1,
                         double& gamma0_1,
                         double& gamma1_1,
                         double& gamma2_1,
                         double& gamma3_1,
                         double& kappa0_1,
                         double& kappa1_1,
                         double& kappa2_1,
                         double& kappa3_1,
                         double& logP1_1,
                         double& logRho0_1,
                         double& logRho1_1,
                         double& logRho2_1,
                         int& npeos_2,
                         double& gamma0_2,
                         double& gamma1_2,
                         double& gamma2_2,
                         double& gamma3_2,
                         double& kappa0_2,
                         double& kappa1_2,
                         double& kappa2_2,
                         double& kappa3_2,
                         double& logP1_2,
                         double& logRho0_2,
                         double& logRho1_2,
                         double& logRho2_2,
                         char (& eos_table1)[500],
                         char (& eos_table2)[500] ) ;


	/** Save in a binary file.
	 *  This file can be subsenquently read by the evolution code,
	 *  or by the constructor {\tt Bin\_NS::Bin\_NS(FILE* )}.
	 */
	void save_bin(FILE* ) const ;

	/** Save in a formatted file.
	 *  This file can be subsenquently read by the evolution code,
	 *  or by the constructor {\tt Bin\_NS::Bin\_NS(ifstream\& )}.
	 */
	void save_form(ofstream& ) const ;

	/// Display
	friend ostream& operator<<(ostream& , const Bin_NS& ) ;

};

}
#endif
